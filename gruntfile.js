module.exports = function(grunt) {
	var config = {
		ftpServer: 't7',
		ftpProd: {
			host: "",
			port: 21,
			username: "",
			password: "",
			path: ".",
		},
		sourcePHPpath:[
		],
		build:{
			minFile: true,
			portfNum: false
		}
	};
	var patterns = [
		"*.php"
	];
	

	var copyPluginProd = [];

	grunt.initConfig({

		/*---------------------- Less --------------------------*/
		less: {
			production: {
				options: {
					paths: ["css"],
					plugins: [

					],
					modifyVars: {
					imgPath: '"/images/"',
					bgPath: '"/images/bg/"',
					icoPath: '"/images/ico/"'
					}
				},
				files: {
					"css/style.css": "css/less/style.less"
				}
			}
		},
		/*---------------------- Less --------------------------*/






		/*---------------------- Watcher --------------------------*/

		watch: {
			files: {
				files: ['css/less/*.less', 'js/*.js','*.php'],
				tasks: [],
				options: {
					livereload: {}
				},
			}
		},

		/*---------------------- Watcher --------------------------*/




		/*---------------------- Production --------------------------*/
		clean: {
			production: ["production/**"],
			productionf: ["production/*", "!production/images"],
			productionff: ["production/*", "!production/images", "!production/plugins"]
		},

		cssmin: {
			all: {
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.css', '!*.min.css'],
					dest: 'production/css',
					ext: '.min.css'
				}]
			}
		},

		uglify: {
			alljs: {
				files: [{
					expand: true,
					cwd: 'js',
					src: '**/*.js',
					dest: 'production/js',
					ext: '.min.js'
				}]
			}
		},

		imagemin: {                          
			allmin: {                       
				options: {                     
					optimizationLevel: 4,
					progressive: true,
				},
				files: [{
					expand: true,                  
					cwd: 'images/',                  
					src: ['**/*.{png,jpg,gif}'], 
					dest: 'production/images/'              
					}]
			}
		},
		copy: {
			main: {
				files: [
					{expand: true, src: ['files/**'], dest: 'production/'},
					{expand: true, src: ['fonts/**'], dest: 'production/'},
					{expand: true, src: ['images/**.svg'], dest: 'production/'},
					{expand: true, src: ['*.*', "!gruntfile.js", "!package.json", "!.bowerrc", "!.ftpauth", "!.gitignore"], dest: 'production/'},
				],
			},
			images: {
				files: [
					{expand: true, src: ['images/**'], dest: 'production/'},
				],
			},
			files: {
				files: [
					{expand: true, src: ['*.*', "!gruntfile.js", "!package.json", "!.bowerrc", "!.ftpauth", "!.gitignore"], dest: 'production/'},
				],
			},
			plugins: {
				files: [
					{expand: true, src: copyPluginProd, dest: 'production/'},
				],
			},
			sourcePHP:{
				files: [
					{expand: true, src:config.sourcePHPpath, dest: 'production/'},
				]
			},

			css:{
				files: [
					{
						expand: true,
						cwd: 'css',
						src: ['*.css', '!*.min.css'],
						dest: 'production/css',
						ext: '.min.css'
					}
				]
			},
			js: {
				files: [{
						expand: true,
						cwd: 'js',
						src: '**/*.js',
						dest: 'production/js',
						ext: '.min.js'
					}]
			},
		},

		htmlmin: {                                     
			production: {                                     
				options: {                                
					removeComments: true,
					collapseWhitespace: true,
					conservativeCollapse: true,
				},
				files: {                                  
					'production/index.php': 'production/index.php',
				}
			},
		},

		/*---------------------- Production --------------------------*/







		/*---------------------- Favicons --------------------------*/

		favicons: {
			options: {
				trueColor: true,
				precomposed: true,
				appleTouchBackgroundColor: "#fff",
				coast: true,
				windowsTile: true,
				tileBlackWhite: false,
				tileColor: "#444",
				html: 'production/index.php',
				HTMLPrefix: "images/icons/"
			},
			icons: {
				src: 'images/favicon.png',
				dest: 'production/images/icons'
			}
		},

		/*---------------------- Favicons --------------------------*/


		/*---------------------- Ftp push --------------------------*/
		ftp_push: {
			buildTest: {
				options: {
					authKey: config.ftpServer,
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: ["**"], dest: "/"}]
			},
			buildTestf: {
				options: {
					authKey: config.ftpServer,
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: [ "**", '!images*/**/*', '!images', "!files/**/*.{mp4,ogv,webm}"], dest: "/"}]
			},
			buildTestff: {
				options: {
					authKey: config.ftpServer,
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: [ "**", '!images*/**/*', '!images', '!plugins/**/*', '!plugins', '!files/**/*', '!files', '!fonts*/**/*', '!fonts'], dest: "/"}]
			},



			buildProd: {
				options: {
					username: config.ftpProd.username,
					password: config.ftpProd.password,
					host: config.ftpProd.host,
					dest: config.ftpProd.path,
					port: config.ftpProd.port
				},
				files: [{expand: true, cwd: 'production', src: ["**"], dest: "/"}]
			},
			buildProdf: {
				options: {
					username: config.ftpProd.username,
					password: config.ftpProd.password,
					host: config.ftpProd.host,
					dest: config.ftpProd.path,
					port: config.ftpProd.port
				},
				files: [{expand: true, cwd: 'production', src: ["**", '!images*/**/*', '!images', "!files/**/*.{mp4,ogv,webm}"], dest: "/"}]
			},
			buildProdff: {
				options: {
					username: config.ftpProd.username,
					password: config.ftpProd.password,
					host: config.ftpProd.host,
					dest: config.ftpProd.path,
					port: config.ftpProd.port
				},
				files: [{expand: true, cwd: 'production', src: ["**", '!images*/**/*', '!images', '!plugins/**/*', '!plugins', '!files/**/*', '!files', '!fonts*/**/*', '!fonts'], dest: "/"}]
			},
		







			buildPortfolio: {
				options: {
					authKey: "portfolio",
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: ["**"], dest: "/"+config.build.portfNum+"/"}]
			},
			buildPortfoliof: {
				options: {
					authKey: "portfolio",
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: ["**", '!images*/**/*', '!images', "!files/**/*.{mp4,ogv,webm}"], dest: "/"+config.build.portfNum+"/"}]
			},
			buildPortfolioff: {
				options: {
					authKey: "portfolio",
					host: "feelbook.ftp.ukraine.com.ua",
					dest: ".",
					port: 21
				},
				files: [{expand: true, cwd: 'production', src: ["**", '!images*/**/*', '!images', '!plugins/**/*', '!plugins', '!files/**/*', '!files', '!fonts*/**/*', '!fonts'], dest: "/"+config.build.portfNum+"/"}]
			}
		},
		/*---------------------- Ftp push --------------------------*/


	});


	/*---------------------- Load components --------------------------*/

	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-favicon-awindows');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-ftp-push');

	/*---------------------- Load components --------------------------*/

	





	/*---------------------- Create tasks --------------------------*/

	grunt.registerTask('default', ["watch"]);
	grunt.registerTask('copy_plugins', ["copy_plugin", "copy:plugins"]);

	grunt.registerTask('prod', ["start","clean:production", "less:production", "copy:main", "copy_plugins", "favicons", "indexedit"]);
	grunt.registerTask('prodf', ["start","clean:productionf", "less:production", "cssmin:all", "uglify:alljs", "copy:main", "copy_plugins", "favicons", "indexedit"]);
	grunt.registerTask('prodff', ["start","clean:productionff", "less:production", "cssmin:all", "uglify:alljs", "copy:files", "favicons", "indexedit"]);

	grunt.registerTask('ftp', ["ftp_push:buildTest"]);
	grunt.registerTask('ftpf', ["ftp_push:buildTestf"]);
	grunt.registerTask('ftpff', ["ftp_push:buildTestff"]);

	grunt.registerTask('ftpp', ["ftp_push:buildProd"]);
	grunt.registerTask('ftppf', ["ftp_push:buildProdf"]);
	grunt.registerTask('ftppff', ["ftp_push:buildProdff"]);


	/*---------------------- Create tasks --------------------------*/

	
















		/*---------------------- My Tasks --------------------------*/
	grunt.registerTask('collect',"This task have 4 options:\n Strength: -s1,-s2,-s3. -s1 Defalut\n Upload on a server: -test, -prod, -pf=x", function(){
		var task = [];


		var fun = function(param){
			if (!grunt.option("dr")) {
				task.push("prod"+param);
			};


			if (grunt.option("img")) {
				task.push("imagemin:allmin");
			}else{
				task.push("copy:images");
			}


			if (config.build.minFile) {
				task.push("cssmin:all");
				task.push("uglify:alljs");
			}else{
				task.push("copy:css");
				task.push("copy:js");
			}


			if (grunt.option("test")) {
				task.push("ftp"+param);
			}
			if (grunt.option("prod")) {
				task.push("ftpp"+param);
			}


			if (grunt.option("pf")) {
				if (!config.build.portfNum) {
					grunt.fail.fatal("Portfolio num is not correct");
				}else{
					task.push("ftp_push:buildPortfolio"+param);
				}
			}
		}


		if(grunt.option("s2")){
			fun("f")
		}else if(grunt.option("s3")){
			fun("ff")
		}else{
			fun("")
		}
		grunt.log.writeln(task);
		grunt.task.run(task);
	});


	var sourcePHPpath = false;

	grunt.registerTask('start', function(){
		

		for (var i = config.sourcePHPpath.length - 1; i >= 0; i--) {
			config.sourcePHPpath[i] = config.sourcePHPpath[i]+"/**/*.php"
		};
		sourcePHPpath = patterns.concat(config.sourcePHPpath);
		
	});



	grunt.registerTask('indexedit', function(){
		grunt.task.run(["copy:sourcePHP", "sourcePHPpath"]);
		
	});




	grunt.registerTask('sourcePHPpath', function(){
		var cheerio = require("cheerio");
		grunt.file.expand({}, sourcePHPpath).forEach(function(path){

			var fileName = "production/"+path;
			console.info(fileName);

			 var $ = cheerio.load(grunt.file.read(fileName),{
					decodeEntities: false,
			});

			$('\n\t<link rel="stylesheet" type="text/css" href="/css/style.min.css">\n').insertAfter('head title');

			$("script[src^='/js/']").not("script[src$='.min.js']").each(function(){
				var desired = $(this).attr("src").replace(/.js$/g, '')+".min.js";
				$(this).attr("src", desired);
			})

			$("link[href^='/css/']").not("link[href$='.min.css']").each(function(){
				var desired = $(this).attr("href").replace(/.css$/g, '')+".min.css";
				$(this).attr("href", desired);
			})

			$(".test-script-tmp").remove();

			grunt.file.write(fileName,$.html())
		})
	});

	grunt.registerTask('copy_plugin', function(){
		var dataArray = [];


		var cheerio = require("cheerio");
		grunt.file.expand({}, sourcePHPpath).forEach(function(path){

			var fileName = "production/"+path;
			console.info(fileName);

			 var $ = cheerio.load(grunt.file.read(fileName),{
					decodeEntities: false,
			});

			
			$("script[src^='/plugins/']").each(function(index, el) {
				var data = $(this).attr("src");
				data = data.replace(/\/plugins\//g, "");
				while(/.{0,}(?=\/)/g.exec(data)){
					data = /.{0,}(?=\/)/g.exec(data)[0];
					
				}
				dataArray.push(data);
			});
			 
		})
		Array.prototype.removeDuplicates = function (){
			var temp=new Array();
			label:for(i=0;i<this.length;i++){
						for(var j=0; j<temp.length;j++ ){//check duplicates
								if(temp[j]==this[i])//skip if already present 
									 continue label;      
						}
						temp[temp.length] = this[i];
			}
			return temp;
		 }

		dataArray = dataArray.removeDuplicates();

		for (var i = 0; i < dataArray.length; i++) {
			copyPluginProd.push("plugins/"+dataArray[i]+"/**");
		};

	});

	/*---------------------- My Tasks --------------------------*/

	(grunt.option.flags()
		.join('')
		.match(/([A-Za-z0-9]+)/g) || [])
		.forEach(function(flag) {
			grunt.option(flag, true)
	});
}
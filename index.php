<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Landing</title>
	<meta name="viewport" content="width=1100">
	<!-- split -->
		<?php 
			include_once 'plugins/aSplit/assets/site_include.php';
			$svariants = array(array($URL,$_SERVER["QUERY_STRING"]),"/");
			$svariants = json_encode($svariants);
			$svariants = urlencode($svariants);
		?>
		<script type="text/javascript" src="/plugins/aSplit/assets/artsites.spli.php?data=<?php echo $svariants; ?>"></script>
	<!-- split -->
	<link rel="stylesheet" type="text/css" href="/fonts/font.css">
	<link rel="stylesheet" type="text/css" href="/css/animate.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">

	<script type="text/javascript" src="/plugins/jquery/dist/jquery.min.js"></script>

	<link rel="stylesheet" type="text/less" href="/css/less/style.less" class="test-script-tmp">
	<script type="text/javascript" src="plugins/less/dist/less.min.js" class="test-script-tmp"></script>
	<script src="//localhost:35729/livereload.js" class="test-script-tmp"></script>
	<script type="text/javascript" class="test-script-tmp">
	$(function(){
		//$(".contact-popup").bPopup();
	})
	</script>
	

	<!-- Forms -->
		<script type="text/javascript" src="/plugins/aSendForm/assets/jquery.validate.min.js"></script>
		<script type="text/javascript" src="/plugins/aSendForm/assets/additional-methods.min.js"></script>
		<script type="text/javascript" src="/plugins/aSendForm/assets/jquery.bpopup.min.js"></script>
		<script type="text/javascript" src="/plugins/aSendForm/assets/jquery.send.form.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/plugins/aSendForm/assets/a-valid.css">
	<!-- Forms -->

	<!-- CountDown -->
		<script type="text/javascript" src="/plugins/coundown/jquery.plugin.min.js"></script>
		<script type="text/javascript" src="/plugins/coundown/jquery.countdown.js"></script>
		<script type="text/javascript" src="/plugins/coundown/jquery.countdown-ru.js"></script>
		<script type="text/javascript" src="/plugins/coundown/jquery.cookie.js"></script>
		<link rel="stylesheet" type="text/css" href="/plugins/coundown/jquery.countdown.css">
	<!-- CountDown -->

	<!-- Backround -->
		<script type="text/javascript" src="/plugins/vide/dist/jquery.vide.min.js"></script>
	<!-- Backround -->


	<!-- Scroll -->
		<script type="text/javascript" src="/plugins/ScrollMagic/scrollmagic/minified/plugins/EasePack.min.js"></script>
		<script type="text/javascript" src="/plugins/ScrollMagic/scrollmagic/minified/plugins/TweenLite.min.js"></script>
		<script type="text/javascript" src="/plugins/ScrollMagic/scrollmagic/minified/ScrollMagic.min.js"></script>
		<script type="text/javascript" src="/plugins/ScrollMagic/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
		<script type="text/javascript" src="/plugins/ScrollMagic/scrollmagic/minified/plugins/ScrollToPlugin.min.js"></script>
		<script type="text/javascript" class="test-script-tmp" src="/plugins/ScrollMagic/scrollmagic/minified/plugins/debug.addIndicators.min.js"></script>
	<!-- Scroll -->


	<!-- geo -->
		<?php define ('geoip', "/plugins/aGeo/assets/geoIP/GeoIP.dat"); include_once '/plugins/aGeo/assets/geoIP/contry.php'; ?>
		<script type="text/javascript">geoContry = '<?php echo getClientContry(); ?>'</script>
		<script type="text/javascript" src="/plugins/aGeo/assets/jquery.geo.js"></script>
	<!-- geo -->

	<!-- target -->
		<script type="text/javascript" src="/plugins/aMult/assets/database.js"></script>
		<script type="text/javascript" src="/plugins/aMult/assets/jquery.cookie.js"></script>
		<script type="text/javascript" src="/plugins/aMult/assets/jquery.mult.js"></script>
	<!-- target -->

	<script type="text/javascript" src="/js/helpers.js"></script>
	<script type="text/javascript" src="/js/script.js"></script>

</head>
<body>

</body>
</html>

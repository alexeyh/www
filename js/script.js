(function(){
	$(document).ready(function(){
	})


	function formSender () {
		var asoc = {
			'name' : 'Имя',
			'phone' : 'Номер телефона',
			'email' : 'Е-mail',
			'wyn': 'Чего хочет клиент'
		};

		var mail = 'aleks300395@mail.ru';

		var vRules = {
				rules: {
					name: {
						required: true
					},
					email: {
						required: false,
						email: true
					},
					phone: {
						required: true,
						regex: /^([\(\)\+\- ]{0,2}[\d]){10,13}$/g
					}
				},

				messages: {
					name: {
						required: "Это поле обязательное для заполнения",
						email: "Введите данные в указаном формате"
					},
					email: {
						email: "Введите данные в указаном формате"
					},
					phone:{
						required: "Это поле обязательное для заполнения",
						regex : 'Номер нужно указывать в международном формате<br><span>+1 234 567 89 01</span>'
					}
				},
			};


		$("#popup-kp").aSendForm({
				popup : ['.pkp', '.btnp-kp'],
				goal : function(){
					// ga('send', 'event', 'knopka', 'zakazat');
					// yaCounter26593851.reachGoal('send');
				},
				postQuery: '/plugins/aSendForm/assets/handle.php',
				closeData: 113000,
				associations : asoc,
				mailTo : mail,
				//answer: true,
				validateRuls: vRules,
				onClickPopup: function(){
					
				},
		});
	}

	function videoBG() {
		$(".head").vide({
		    mp4: "/files/video/bg.mp4",
		    webm: "/files/video/bg.webm",
		    ogv: "/files/video/bg.ogv",
		    poster: "/images/bg/video/bg.jpg"
		}, {
		
		});
	}


	function conversePlus () {
		$.aGeo({
			input : "#contry",
			inputDef : "#contry",
			defText : '22',	
		});
		$.aMult({
			//input : ".head h1",
			dataBase : dataBaseTarget,
			getRequest : "key",
			defPhrase : "Хотите увеличить продажи и вывести Ваш бизнес на новый уровень?",
		});
	}



	function gmap () {
		$(".contacts .map").gmap3({
		marker:{
		   values:[
		      {latLng:[50.0605411, 19.9584616], data:"Addres"},
		    ],
		},
		map:{
		  options:{
			zoom: 14,
			// navigationControl: false,
			scrollwheel: false,
	        // streetViewControl: false,
	        // overviewMapControl: false,
	        panControl: false,
	        zoomControl: false,
	        streetViewControl: false,

		  }
		}
	  });
	}
})()
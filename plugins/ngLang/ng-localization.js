$lozObj = {};
$lozObj.event = [];
(function(){
	var localiz = angular.module("localizator",['ipCookie','ngSanitize']);

	localiz.controller('localizController', ['$scope', 'Product', 'ipCookie', '$sce', function($scope, Product, ipCookie, $sce){
		
		//$scope.languages = $scope.languages | "ru";
		$scope.$watch('languages', function(newValue) {
			
			Product(newValue).then(function(data){
				$scope.loz   = data.data;
				$lozObj.lang = $scope.languages;
				$lozObj.loz  = data.data;
				if ($lozObj.event) {
					for(var name in $lozObj.event){
						window[$lozObj.event[name]]();
					}
				};
			});
		});


		function getParameterByName (name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		};
		
		var getLang = getParameterByName("lang");
		var cookieLang = '';
		if(getLang){
			ipCookie('lang', getLang, {expires: 365});
			$scope.languages = getLang;
		}else if (cookieLang = ipCookie('lang')) {
			$scope.languages = cookieLang;
		}else{
			ipCookie('lang', $scope.languagesDef, {expires: 365});
			$scope.languages = $scope.languagesDef;
			console.log($scope.languagesDef);
		}


		$scope.safeHTML = function(element){
			return $sce.trustAsHtml($scope.snippet);
		};
				


		$scope.changeLoz = function(nLoz){
				$scope.languages = nLoz;
				ipCookie('lang', $scope.languages, {expires: 365});
		};
	}])

	// function localizController ($scope, $element, $attrs, Product, ipCookie) {
		
	
	// };
	
	localiz.factory('Product', ['$http', function($http) {
   		return function(loz) {
   			 return $http.get("/plugins/ngLang/base/"+loz+".json");
   		}
	   
	    
	}]);

	localiz.directive('localizateD', function(){
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			// scope: {}, // {} = isolate, true = child, false/undefined = no change
			controller: 'localizController',
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			// template: '',
			// templateUrl: '',
			// replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
			// link: function($scope, iElm, iAttrs, controller) {
				
			// }
		};
	});
}())